<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/app/example", name="homepage")
     */
    public function indexAction(Request $request)
    {	$arr_per = array();
    	$arr_per[] = array('nombre'=>'diego');
    	$arr_per[] = array('nombre'=>'pame');
    	$arr_per[] = array('nombre'=>'peperoni');
    	$arr_per[] = array('nombre'=>'mascardoni');

        $session = $request->getSession();
        $session->set('lang','esp');

        $this->addFlash('msg','Standard flash message');

        return $this->render('default/index.html.twig',array('page_title'=>'First symfony','arr_per'=>$arr_per));
    }

    public function footerAction(){

        return $this->render('default/footer.html.twig',array('footer_text'=>'Footer from another controller'));
    }

    /**
     * @Route("app/contact", name="contact")
     */
    public function contactAction(Request $request){
        $str_mail = 'diego.gpl.free@gmail.com';
        $session =$request->getSession();
        $lang = $session->get('lang','eng');

        $name = 'Diego';
        
        return $this->render('default/contact.html.twig',array('mail'=>$str_mail,'name'=>$name,'lang'=>$lang));
    }

    /**
     * @Route("app/raw/name/{name}/last/{last}.{_format}",
     *          defaults={"_format"="html"},
     *          requirements = { "_format" = "html|xml" },
     *          name="raw")
     */
    public function rawAction($name,$last,$_format,Request $request){

        //throw $this->createNotFoundException();
        //throw new \Exception('An error in the fail has appears!');
        //return $this->redirectToRoute('contact',array('name'=>$name));



        $edad = $request->query->get('age');
        $sexo = $request->request->get('genere');

        $entry = array('name'=>$name,'last'=>$last,'age'=>$edad,'genere'=>$sexo);
        $response = array('edad'=>'31');
        $arr_response = array('entry'=>$entry,'response'=>$response);
        
        return $this->render('default/raw.'.$_format.'.twig',$entry);
        //return new Response(json_encode($arr_response));
    }
}
