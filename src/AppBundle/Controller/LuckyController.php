<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class LuckyController extends Controller
{
    /**
     * @Route("/lucky/number/{count}", name="lucky")
     */
    public function numberAction($count)
    {	
        $str_luckynumbers = '';
        $str_sep='';
        for($int_i=0;$int_i<(int)$count; $int_i++){
            $str_luckynumbers .= $str_sep.rand(0,100);
            $str_sep=',';
        }        

        return new Response('<html><body>TEST: '.$str_luckynumbers.'</body></html>');
    }    

    /**
     * @Route("/lucky/number/template/{count}", name="lucky_template")
     */
    public function luckytemplateAction($count)
    {   
        $str_luckynumbers = '';
        $str_sep='';
        for($int_i=0;$int_i<(int)$count; $int_i++){
            $str_luckynumbers .= $str_sep.rand(0,100);
            $str_sep=',';
        }        

        $view_data = array('luckynumber'=>$str_luckynumbers);

        return $this->render('lucky/number.html.twig',$view_data);

        /*$html = $this->container->get('templating')->render('lucky/number.html.twig',$view_data);

        return new Response($html);*/
    }  

    /**
     * @Route("/api/lucky/number", name="api_lucky")
     */
    public function apiNumberAction(Request $request)
    {   
        $response = array('number'=>rand(0,100));

        return new JsonResponse($response);

        return new Response(json_encode($response),
                                200,
                                array('Content-Type'=>'application/json'));
    }
}
